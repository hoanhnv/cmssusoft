//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Susoft.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Products
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageTitle { get; set; }
        public Nullable<int> Platform { get; set; }
        public string Scope { get; set; }
        public Nullable<System.DateTime> FinishedDate { get; set; }
        public string ImagePath1 { get; set; }
        public string ImagePath2 { get; set; }
        public string ImagePath3 { get; set; }
        public string ImagePath4 { get; set; }
        public string ImagePath5 { get; set; }
        public string Description { get; set; }
        public string Summary { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<int> Status { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public string UpdateBy { get; set; }
        public Nullable<System.DateTime> UpdateAt { get; set; }
        public string Name_EN { get; set; }
        public string Summary_EN { get; set; }
        public string Description_EN { get; set; }
        public string Name_JP { get; set; }
        public string Summary_JP { get; set; }
        public string Description_JP { get; set; }
        public string URL { get; set; }
    }
}
