﻿using Susoft.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Susoft.Data.Configuration
{
    public class ArticlesConfiguration : EntityTypeConfiguration<Articles>
    {
        public ArticlesConfiguration()
        {
            ToTable("Articles");
            //Property(g => g.Title).IsRequired().HasMaxLength(50);
            //Property(g => g.Title).IsRequired().HasPrecision(8, 2);
            //Property(g => g.Id).IsRequired();
        }
    }
}
