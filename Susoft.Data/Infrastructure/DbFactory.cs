﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Susoft.Model;
namespace Susoft.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        SusoftCMSEntities dbContext;

        public SusoftCMSEntities Init()
        {
            return dbContext ?? (dbContext = new SusoftCMSEntities());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
