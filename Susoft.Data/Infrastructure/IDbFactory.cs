﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Susoft.Model;
namespace Susoft.Data.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        SusoftCMSEntities Init();
    }
}
