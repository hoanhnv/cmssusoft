﻿using Susoft.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Susoft.Model;

namespace Susoft.Data.Repositories
{
    public class CategoryRepository : RepositoryBase<Category>, ICategoryRepository
    {
        public CategoryRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        public IList<Category> GetAllCateForMenu(int parentId, int position, bool isMenu)
        {
            throw new NotImplementedException();
        }

        public Category GetCategoryByName(string categoryName)
        {
            var category = this.DbContext.Category.Where(c => c.Name == categoryName).FirstOrDefault();

            return category;
        }

        public IList<Category> GetCategoryByParrentId(int parentId)
        {
            throw new NotImplementedException();
        }

        public override void Update(Category entity)
        {
            entity.UpdateAt = DateTime.Now;
            base.Update(entity);
        }
    }

    public interface ICategoryRepository : IRepository<Category>
    {
        Category GetCategoryByName(string categoryName);
        IList<Category> GetAllCateForMenu(int parentId, int position, bool isMenu);
        IList<Category> GetCategoryByParrentId(int parentId);
    }
}
