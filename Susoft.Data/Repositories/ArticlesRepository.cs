﻿using Susoft.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Susoft.Model;
namespace Susoft.Data.Repositories
{
    public interface IArticlesRepository : IRepository<Articles>
    {
    }
    public class ArticlesRepository : RepositoryBase<Articles>, IArticlesRepository
    {
        public ArticlesRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
        public override void Update(Articles entity)
        {
            entity.UpdateAt = DateTime.Now;
            base.Update(entity);
        }
    }
}
