﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Susoft.Model;
using Susoft.Data;
using Susoft.Data.Infrastructure;
using Susoft.Data.Repositories;

namespace Susoft.Service
{

    public interface IArticleService
    {
        IEnumerable<Articles> GetArticles(string name = null);
        Articles GetArticles(int id);
        void CreateArticles(Articles article);
        void SaveArticles();
    }
    public class ArticleService
    {
        private readonly ArticlesRepository articleRepository;
        private readonly IUnitOfWork unitOfWork;
    }
}
