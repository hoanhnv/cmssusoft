﻿using Susoft.Data.Infrastructure;
using Susoft.Data.Repositories;
using Susoft.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Susoft.Service
{
    // operations you want to expose
    public interface ICategoryService
    {
        IEnumerable<Category> GetCategories(string name = null);
        Category GetCategory(int id);
        Category GetCategory(string name);
        void CreateCategory(Category category);
        void SaveCategory();
        // For Bussiness
        IList<Category> GetAllCateForMenu(int parentId, int position, bool isMenu);
        IList<Category> GetCategoryByParrentId(int parentId);
    }

    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository categorysRepository;
        private readonly IUnitOfWork unitOfWork;

        public CategoryService(ICategoryRepository categorysRepository, IUnitOfWork unitOfWork)
        {
            this.categorysRepository = categorysRepository;
            
            this.unitOfWork = unitOfWork;
        }

        #region ICategoryService Members

        public IEnumerable<Category> GetCategories(string name = null)
        {
            if (string.IsNullOrEmpty(name))
                return categorysRepository.GetAll();
            else
                return categorysRepository.GetAll().Where(c => c.Name == name);
        }

        public Category GetCategory(int id)
        {
            var category = categorysRepository.GetById(id);
            return category;
        }

        public Category GetCategory(string name)
        {
            var category = categorysRepository.GetCategoryByName(name);
            return category;
        }

        public void CreateCategory(Category category)
        {
            categorysRepository.Add(category);
        }

        public void SaveCategory()
        {
            unitOfWork.Commit();
        }

        public IList<Category> GetAllCateForMenu(int parentId, int position, bool isMenu)
        {
           return categorysRepository.GetAllCateForMenu(parentId, position, isMenu);
        }

        public IList<Category> GetCategoryByParrentId(int parentId)
        {
            return categorysRepository.GetCategoryByParrentId(parentId);
        }

        #endregion
    }
}
