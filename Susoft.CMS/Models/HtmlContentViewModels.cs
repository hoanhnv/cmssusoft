﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Susoft.CMS.Models
{
    public class HtmlContentViewModels
    {
        [AllowHtml]
        public string HtmlContent1 { get; set; } 
        public int PageId { get; set; }
        public int Id { get; set; }
    }
}