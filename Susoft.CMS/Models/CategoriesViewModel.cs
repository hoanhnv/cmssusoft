﻿using Susoft.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Susoft.CMS.Models
{
    public class CategoriesViewModel
    {
        public Category categoryInfo { get; set; }
        public Category parentCategory { get; set; }
        public List<Category> childCategory { get; set; }

    }
}