﻿using Susoft.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Susoft.CMS.Models
{
    public class ProductViewModel
    {
        public Products productInfo { get; set; }
        public List<SelectListItem> statusProduct { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        [AllowHtml]
        public string Description_EN { get; set; }
        [AllowHtml]
        public string Description_JP { get; set; }
        [AllowHtml]
        public string Summary { get; set; }
        [AllowHtml]
        public string Summary_EN { get; set; }
        [AllowHtml]
        public string Summary_JP { get; set; }
    }
}