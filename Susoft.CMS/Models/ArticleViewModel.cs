﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Susoft.Model;
namespace Susoft.CMS.Models
{
    public class ArticleViewModel
    {
        public Articles articleInfo { get; set; }
        public List<SelectListItem> statusArticles { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        [AllowHtml]
        public string Description_EN { get; set; }
        [AllowHtml]
        public string Description_JP { get; set; }

        [AllowHtml]
        public string Summary { get; set; }
        //public bool isHotNew { get; set; }
        [AllowHtml]
        public string Summary_EN { get; set; }
        [AllowHtml]
        public string Summary_JP { get; set; }
    }
}