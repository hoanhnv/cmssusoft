﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Susoft.CMS.Models
{
    public class ImageViewModel
    {
        public string Url { get; set; }
    }
}