﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Susoft.CMS.Common
{
    public static class LanguageHelper
    {
        public static MvcHtmlString LangSwitcher(this UrlHelper url, string imgName, RouteData routeData, string lang)
        {
            var aTagBuilder = new TagBuilder("a");
            string strImage = @"<img src=""" + imgName + @"""/>"; ;

            var routeValueDictionary = new RouteValueDictionary(routeData.Values);
            if (routeValueDictionary.ContainsKey("lang"))
            {
                string strLang = routeData.Values["lang"] as string;
                if (strLang.Contains(lang))
                {
                    strImage = @"<img src=""" + imgName + @""" width=""36px""/>";
                }
                else
                {
                    strImage = @"<img src=""" + imgName + @"""/>";
                    routeValueDictionary["lang"] = lang;
                }
            }
           
            aTagBuilder.MergeAttribute("href", url.RouteUrl(routeValueDictionary));
            aTagBuilder.InnerHtml = strImage;
            return new MvcHtmlString(aTagBuilder.ToString());
        }
    }
}