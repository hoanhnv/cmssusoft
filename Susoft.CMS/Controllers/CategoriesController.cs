﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Susoft.Model;
using System.Web.Mvc.Html;
using Susoft.CMS.Common;
using Susoft.CMS.Models;

namespace Susoft.CMS.Controllers
{
    [Authorize]
    public class CategoriesController : Controller
    {
        private SusoftCMSEntities db = new SusoftCMSEntities();

        // GET: Categories
        public ActionResult Index()
        {
            var lstData = db.Category.ToList();
            var lstNew = new List<CategoriesViewModel>();
            foreach(Category cate in lstData)
            {
                var cateViewModel = new CategoriesViewModel();
                cateViewModel.categoryInfo = cate;
                cateViewModel.parentCategory = db.Category.Find(cate.ParentId);   
                lstNew.Add(cateViewModel);
            }
            return View(lstNew);
        }
        [ChildActionOnly]
        public Category getCategory(int cateId)
        {
            Category category = db.Category.Find(cateId);
            return category;
        }
        // GET: Categories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Category.Find(id);
            var cateModel = new CategoriesViewModel();
            cateModel.categoryInfo = category;
            cateModel.parentCategory = db.Category.Find(category.ParentId);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(cateModel);
        }

        // GET: Categories/Create
        public ActionResult Create()
        {
            ViewBag.Position = Utitlitys.PositionToList();
            ViewBag.Status = Utitlitys.StatusToList();
            ViewBag.CategoryId = new SelectList(db.Category.ToList(), "Id", "Name");
            return View();
        }
        
        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoriesViewModel category, FormCollection cl)
        {
            if (ModelState.IsValid)
            {
                category.categoryInfo.CreateAt = DateTime.Now;
                category.categoryInfo.CreateBy = User.Identity.Name;
                if (cl["IsMenu"]!=null&&(cl["IsMenu"]).ToString().Equals("on"))
                {
                    category.categoryInfo.IsMenu = true;
                } 
                else
                {
                    category.categoryInfo.IsMenu = false;
                }
                if (category.categoryInfo.ParentId == null)
                {
                    category.categoryInfo.ParentId = -1;
                }
                db.Category.Add(category.categoryInfo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(category);
        }

        // GET: Categories/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.Position = Utitlitys.PositionToList();
            ViewBag.Status = Utitlitys.StatusToList();
            ViewBag.CategoryId = new SelectList(db.Category.ToList(), "Id", "Name");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Category.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            CategoriesViewModel viewModel = new CategoriesViewModel();
            viewModel.categoryInfo = category;
            return View(viewModel); 
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CategoriesViewModel category, FormCollection cl)
        {
            if (ModelState.IsValid)
            {
                category.categoryInfo.UpdateAt = DateTime.Now;
                category.categoryInfo.UpdateBy = User.Identity.Name;
                if (cl["IsMenu"]!=null && (cl["IsMenu"]).ToString().Equals("on"))
                {
                    category.categoryInfo.IsMenu = true;
                }
                else
                {
                    category.categoryInfo.IsMenu = false;
                }
                if (category.categoryInfo.ParentId == null)
                {
                    category.categoryInfo.ParentId = -1;
                }
                db.Entry(category.categoryInfo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(category);
        }

        // GET: Categories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Category.Find(id);
            var categoryModel = new CategoriesViewModel();
            categoryModel.categoryInfo = category;
            categoryModel.parentCategory = db.Category.Find(category.ParentId);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(categoryModel);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Category category = db.Category.Find(id);
            db.Category.Remove(category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
