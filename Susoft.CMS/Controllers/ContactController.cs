﻿using Amazon.ElasticMapReduce.Model;
using Susoft.CMS.Models;
using Susoft.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
namespace Susoft.CMS.Controllers
{
    public class ContactController : Controller
    {
        private SusoftCMSEntities db = new SusoftCMSEntities();
        // GET: Contact
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public async Task<ActionResult> SendContact(FormCollection fm, string email, string name, string contactMsg, string phoneNo)
        public ActionResult SendContact(FormCollection fm)
        {
            //var body = "<p>Email From: {0} ({1})</p><p>Message:</p><p>{2}</p><p>Phone:</p><p>{3}</p>";
            //var message = new MailMessage();
            //message.To.Add(new MailAddress("info@mobileplus.vn"));
            //message.From = new MailAddress("congtykhoinguon@gmail.com");
            //message.Subject = "Liên hệ";
            //message.Body = string.Format(body, email, name, contactMsg, phoneNo);
            //message.IsBodyHtml = true;

            //var client = new SmtpClient("smtp.gmail.com", 587)
            //{
            //    Credentials = new NetworkCredential("congtykhoinguon@gmail.com", "14121517"),
            //    EnableSsl = true
            //};
            //client.Send("congtykhoinguon@gmail.com", "info@mobileplus.vn", message.Subject, message.Body);

            string mail = fm.Get("email") != null ? fm.Get("email") : "";
            string yourname = fm.Get("name") != null ? fm.Get("name") : "";
            string phone = fm.Get("phoneNo") != null ? fm.Get("phoneNo") : "";
            string msg = fm.Get("contactMsg") != null ? fm.Get("contactMsg") : "";
            LienHe contact = new LienHe();
            contact.Email = mail;
            contact.YourName = yourname;
            contact.Phone = phone;
            contact.Message = msg;
            contact.CreateAt = DateTime.Now;
            db.LienHe.Add(contact);
            db.SaveChanges();
            TempData["Message"] = "Gửi liên hệ thành công!";
            return RedirectToAction("Index");
        }
    }
}
