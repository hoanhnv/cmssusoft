﻿using Susoft.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Susoft.CMS.Controllers
{
    public class ServicesController : Controller
    {
        private SusoftCMSEntities db = new SusoftCMSEntities();
        // GET: Services
        public ActionResult Index()
        {
            HtmlContent htmlContent = db.HtmlContent.Where(x => x.PageId == 2).FirstOrDefault();
            return View(htmlContent);
        }
    }
}