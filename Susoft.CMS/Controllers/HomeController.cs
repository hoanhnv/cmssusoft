﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Susoft.Model;
using Susoft.CMS.Common;
using Susoft.CMS.Models;
using System.Web.Mvc.Html;

namespace Susoft.CMS.Controllers
{
    [Localization("vi")]
    public class HomeController : BaseController
    {
        private SusoftCMSEntities db = new SusoftCMSEntities();
        public ActionResult Index()
        {
            var whatWedo = db.Articles.Include(x =>x.Category).Where(x=>x.Status == (int)CMS_STATUS.ACTIVE && x.HotNew== true && x.CategoryId == 1);
            ViewBag.WhatWeDo = whatWedo.ToList();

            var latestNew = db.Articles.Include(x => x.Category).Where(x => x.Status == (int)CMS_STATUS.ACTIVE&& x.HotNew == true && x.CategoryId == 7);
            ViewBag.LatestNew = latestNew.ToList();

            var sliderNew = db.Articles.Include(x => x.Category).Where(x => x.Status == (int)CMS_STATUS.ACTIVE);
            ViewBag.SliderNew = sliderNew.ToList();
            getDataMenu();
            return View();
        }
        public void getDataMenu()
        {
            var categoryParent = db.Category.Where(x => x.Status == (int)CMS_STATUS.ACTIVE
             && x.IsMenu == true
             && x.ParentId == -1).OrderBy(x => x.OrderBy).ToList(); ;

            var categoryChild = db.Category.Where(x => x.Status == (int)CMS_STATUS.ACTIVE
              && x.IsMenu == true
              && x.ParentId != -1).OrderBy(x => x.OrderBy).ToList();

            var ListCategoryForMenu = new List<CategoriesViewModel>();
            foreach (Category item in categoryParent)
            {
                CategoriesViewModel cateViewModel = new CategoriesViewModel();
                cateViewModel.categoryInfo = item;
                foreach (Category sub in categoryChild)
                {
                    if (cateViewModel.childCategory == null)
                    {
                        cateViewModel.childCategory = new List<Category>();
                    }
                    if (sub.ParentId == item.Id)
                    {
                        cateViewModel.childCategory.Add(sub);
                    }
                }
                ListCategoryForMenu.Add(cateViewModel);
            }

            ViewBag.ListCategoryForMenu = ListCategoryForMenu;
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}