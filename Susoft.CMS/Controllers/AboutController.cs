﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Susoft.Model;
namespace Susoft.CMS.Controllers
{
    public class AboutController : Controller
    {
        private SusoftCMSEntities db = new SusoftCMSEntities();

        // GET: About
        public ActionResult Index()
        {
            HtmlContent htmlContent = db.HtmlContent.Where(x => x.PageId == 1).FirstOrDefault();
            return View(htmlContent);
        }
    }
}