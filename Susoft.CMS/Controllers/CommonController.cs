﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Susoft.Model;
using Susoft.CMS.Common;
using Susoft.CMS.Models;
namespace Susoft.CMS.Controllers
{
    public class CommonController : Controller
    {
        private SusoftCMSEntities db = new SusoftCMSEntities();
        // GET: Common
        public ActionResult Index()
        {
            return View();
        }
        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult GetMainMenu()
        {
            var categoryParent = db.Category.Where(x => x.Status == (int)CMS_STATUS.ACTIVE
              && x.IsMenu == true
              && (x.ParentId == -1||x.ParentId == null)).OrderBy(x => x.OrderBy).ToList(); ;

            var categoryChild = db.Category.Where(x => x.Status == (int)CMS_STATUS.ACTIVE
              && x.IsMenu == true
              && x.ParentId != -1 && x.ParentId != null).OrderBy(x => x.OrderBy).ToList();

            var ListCategoryForMenu = new List<CategoriesViewModel>();
            foreach (Category item in categoryParent)
            {
                CategoriesViewModel cateViewModel = new CategoriesViewModel();
                cateViewModel.categoryInfo = item;
                foreach (Category sub in categoryChild)
                {
                    if (cateViewModel.childCategory == null)
                    {
                        cateViewModel.childCategory = new List<Category>();
                    }
                    if (sub.ParentId == item.Id)
                    {
                        cateViewModel.childCategory.Add(sub);
                    }
                }
                ListCategoryForMenu.Add(cateViewModel);
            }
            ViewBag.ListCategoryForMenu = ListCategoryForMenu;
            return PartialView(ListCategoryForMenu);
        }
        [ChildActionOnly]
        public ActionResult GetSideBar()
        {
            var categoryChild = db.Category.Where(x=>x.Status == (int)CMS_STATUS.ACTIVE 
                && x.IsMenu == true
                && x.ParentId != -1 && 
                (x.Position == (int)MENU_POSITION.LEFT_MENU || x.Position == (int)MENU_POSITION.RIGHT_MENU)).OrderBy(x => x.OrderBy).ToList();
            ViewBag.ListCategoryChild = categoryChild;
            return PartialView(categoryChild);
        }
    }
}