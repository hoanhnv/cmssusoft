﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Susoft.Model;
using Susoft.CMS.Common;
using Susoft.CMS.Models;
using System.Web.Mvc.Html;
using System.IO;

namespace Susoft.CMS.Controllers
{
    [Authorize]
    public class ArticlesController : Controller
    {
        private SusoftCMSEntities db = new SusoftCMSEntities();

        // GET: Articles
        public ActionResult Index()
        {
            var articles = db.Articles.Include(a => a.Category);
            return View(articles.ToList());
        }

        // GET: Articles/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Articles articles = db.Articles.Find(id);
            if (articles == null)
            {
                return HttpNotFound();
            }
            return View(articles);
        }

        // GET: Articles/Create
        public ActionResult Create()
        {
            ViewBag.Status = Utitlitys.StatusToList();
            ViewBag.CategoryId = new SelectList(db.Category, "Id", "Name");
            return View();
        }

        // POST: Articles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(ArticleViewModel articles, IEnumerable<HttpPostedFileBase> files, FormCollection cl)
        {
            if (ModelState.IsValid)
            {
                if (cl["HotNew"]!= null && (cl["HotNew"]).Equals("on"))
                {
                    articles.articleInfo.HotNew = true;
                }
                else
                {
                    articles.articleInfo.HotNew = false;
                }
                var fileName = string.Empty;
                if (files != null)
                {
                    foreach (var file in files)
                    {
                        if (file != null && file.ContentLength > 0)
                        {
                            //kiem tra file extension lan nua
                            List<string> excelExtension = new List<string>();
                            excelExtension.Add(".png");
                            excelExtension.Add(".jpg");
                            excelExtension.Add(".gif");
                            var extension = Path.GetExtension(file.FileName);
                            if (!excelExtension.Contains(extension.ToLower()))
                            {
                                return Json(new { success = 0, message = "File không đúng định dạng cho phép, bạn hãy chọn file có định dạng .jpg, gif hoặc png" });
                            }
                            //kiem tra dung luong file
                            var fileSize = file.ContentLength;
                            var fileMB = (fileSize / 1024f) / 1024f;
                            if (fileMB > 5)
                            {
                                return Json(new { success = 0, message = "Dung lượng ảnh không được lớn hơn 5MB" });
                            }
                            // luu ra dia
                            fileName = Path.GetFileNameWithoutExtension(file.FileName);
                            //fileName = fileName + "_" + user.UserID + extension;
                            fileName = fileName + extension;
                            var physicalPath = Path.Combine(Server.MapPath("~/Uploads/TinTuc"), fileName);
                            file.SaveAs(physicalPath);
                            articles.articleInfo.ThumbURL = fileName;
                        }
                    }
                }
                articles.articleInfo.Summary = articles.Summary;
                articles.articleInfo.Summary_EN = articles.Summary_EN;
                articles.articleInfo.Summary_JP = articles.Summary_JP;
                articles.articleInfo.Description = articles.Description;
                articles.articleInfo.Description_EN = articles.Description_EN;
                articles.articleInfo.Description_JP = articles.Description_JP;
                articles.articleInfo.CreateAt = DateTime.Now;
                articles.articleInfo.CreateBy = User.Identity.Name;
                articles.articleInfo.Category = db.Category.Find(articles.articleInfo.CategoryId);
                //articles.articleInfo.HotNew = articles.isHotNew;
                db.Articles.Add(articles.articleInfo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(db.Category, "Id", "Name", articles.articleInfo.CategoryId);
            return View(articles);
        }

        // GET: Articles/Edit/5
        public ActionResult Edit(long? id)
        {
            ViewBag.Status = Utitlitys.StatusToList();
            ViewBag.CategoryId = new SelectList(db.Category, "Id", "Name");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Articles articles = db.Articles.Find(id);
            if (articles == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = new SelectList(db.Category, "Id", "Name", articles.CategoryId);
            ArticleViewModel viewmodel = new ArticleViewModel();
            viewmodel.articleInfo = articles;
            viewmodel.Description = articles.Description;
            viewmodel.Description_EN = articles.Description_EN;
            viewmodel.Description_JP = articles.Description_JP;
            viewmodel.Summary = articles.Summary;
            viewmodel.Summary_EN = articles.Summary_EN;
            viewmodel.Summary_JP = articles.Summary_JP;
            return View(viewmodel);
        }

        // POST: Articles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ArticleViewModel articles, IEnumerable<HttpPostedFileBase> files, FormCollection cl)
        {
            if (ModelState.IsValid)
            {
                if (cl["HotNew"] != null && (cl["HotNew"]).Equals("on"))
                {
                    articles.articleInfo.HotNew = true;
                }
                else
                {
                    articles.articleInfo.HotNew = false;
                }
                var old = db.Articles.Find(articles.articleInfo.Id);
                var fileName = string.Empty;
                if (files != null)
                {
                    foreach (var file in files)
                    {
                        if (file != null && file.ContentLength > 0)
                        {
                            //kiem tra file extension lan nua
                            List<string> excelExtension = new List<string>();
                            excelExtension.Add(".png");
                            excelExtension.Add(".jpg");
                            excelExtension.Add(".gif");
                            var extension = Path.GetExtension(file.FileName);
                            if (!excelExtension.Contains(extension.ToLower()))
                            {
                                return Json(new { success = 0, message = "File không đúng định dạng cho phép, bạn hãy chọn file có định dạng .jpg, gif hoặc png" });
                            }
                            //kiem tra dung luong file
                            var fileSize = file.ContentLength;
                            var fileMB = (fileSize / 1024f) / 1024f;
                            if (fileMB > 5)
                            {
                                return Json(new { success = 0, message = "Dung lượng ảnh không được lớn hơn 5MB" });
                            }
                            if (old.ThumbURL != null)
                            {
                                fileName = old.ThumbURL;
                            }
                            else
                            {
                                // luu ra dia
                                fileName = Path.GetFileNameWithoutExtension(file.FileName);
                                //fileName = fileName + "_" + user.UserID + extension;
                                fileName = fileName + extension;
                            }

                            var physicalPath = Path.Combine(Server.MapPath("~/Uploads/TinTuc"), fileName);
                            file.SaveAs(physicalPath);
                            articles.articleInfo.ThumbURL = fileName;
                            old.ThumbURL = articles.articleInfo.ThumbURL;
                        }
                    }
                }
                articles.articleInfo.Category = db.Category.Find(articles.articleInfo.CategoryId);
                old.Category = articles.articleInfo.Category;
                old.Status = articles.articleInfo.Status;
                old.HotNew = articles.articleInfo.HotNew;
                old.Title = articles.articleInfo.Title;
                old.Title_EN = articles.articleInfo.Title_EN;
                old.Title_JP = articles.articleInfo.Title_JP;
                old.UpdateAt = DateTime.Now;
                old.UpdateBy = User.Identity.Name;
                old.Summary = articles.Summary;
                old.Description = articles.Description;
                old.Summary_EN = articles.Summary_EN;
                old.Summary_JP = articles.Summary_JP;
                old.Description_EN = articles.Description_EN;
                old.Description_JP = articles.Description_JP;
                db.Entry(old).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");             
            }
            ViewBag.CategoryId = new SelectList(db.Category, "Id", "Name", articles.articleInfo.CategoryId);
            return View(articles);
        }

        // GET: Articles/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Articles articles = db.Articles.Find(id);
            if (articles == null)
            {
                return HttpNotFound();
            }
            return View(articles);
        }

        // POST: Articles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Articles articles = db.Articles.Find(id);
            db.Articles.Remove(articles);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
