﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Susoft.Model;
using Susoft.CMS.Models;

namespace Susoft.CMS.Controllers
{
    [Authorize]
    public class HtmlContentsController : Controller
    {
        private SusoftCMSEntities db = new SusoftCMSEntities();

        // GET: HtmlContents
        public ActionResult Index()
        {
            return View(db.HtmlContent.ToList());
        }

        // GET: HtmlContents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HtmlContent htmlContent = db.HtmlContent.Find(id);
            if (htmlContent == null)
            {
                return HttpNotFound();
            }
            return View(htmlContent);
        }

        // GET: HtmlContents/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HtmlContents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HtmlContentViewModels htmlContent)
        {
            if (ModelState.IsValid)
            {
                var obj = new HtmlContent();
                obj.HtmlContent1 = htmlContent.HtmlContent1;
                obj.PageId = htmlContent.PageId;
                db.HtmlContent.Add(obj);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(htmlContent);
        }

        // GET: HtmlContents/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HtmlContent htmlContent = db.HtmlContent.Find(id);
            if (htmlContent == null)
            {
                return HttpNotFound();
            }
            return View(htmlContent);
        }

        // POST: HtmlContents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HtmlContentViewModels htmlContent)
        {
            if (ModelState.IsValid)
            {
                var old = db.HtmlContent.Find(htmlContent.Id);
                old.HtmlContent1 = htmlContent.HtmlContent1;
                old.PageId = htmlContent.PageId;
                db.Entry(old).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(htmlContent);
        }

        // GET: HtmlContents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HtmlContent htmlContent = db.HtmlContent.Find(id);
            if (htmlContent == null)
            {
                return HttpNotFound();
            }
            return View(htmlContent);
        }

        // POST: HtmlContents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HtmlContent htmlContent = db.HtmlContent.Find(id);
            db.HtmlContent.Remove(htmlContent);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
