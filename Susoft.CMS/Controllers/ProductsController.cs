﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Susoft.Model;
using Susoft.CMS.Common;
using Susoft.CMS.Models;
using CKSource.FileSystem;

namespace Susoft.CMS.Controllers
{
    [Authorize]
    public class ProductsController : Controller
    {
        private SusoftCMSEntities db = new SusoftCMSEntities();
     

        // GET: Products
        public ActionResult Index()
        {
            return View(db.Products.ToList());
        }
        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Products products = db.Products.Find(id);
            if (products == null)
            {
                return HttpNotFound();
            }
            return View(products);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            ViewBag.ProductType = Utitlitys.ProductTypeToList();
            ViewBag.Status = Utitlitys.StatusToList();
            ViewBag.Platform = Utitlitys.PlatformToList();
            ViewBag.ProductId = new SelectList(db.Products.ToList(), "Id", "Name");
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductViewModel products, IEnumerable<HttpPostedFileBase> files)
        {
            ViewBag.ProductType = Utitlitys.ProductTypeToList();
            ViewBag.Status = Utitlitys.StatusToList();
            ViewBag.Platform = Utitlitys.PlatformToList();
            ViewBag.ProductId = new SelectList(db.Products.ToList(), "Id", "Name");
            if (ModelState.IsValid)
            {
                var fileName = string.Empty;
                if (files != null)
                {
                    foreach (var file in files)
                    {
                        if (file != null && file.ContentLength > 0)
                        {
                            //kiem tra file extension lan nua
                            List<string> excelExtension = new List<string>();
                            excelExtension.Add(".png");
                            excelExtension.Add(".jpg");
                            excelExtension.Add(".gif");
                            var extension = Path.GetExtension(file.FileName);
                            if (!excelExtension.Contains(extension.ToLower()))
                            {
                                return Json(new { success = 0, message = "File không đúng định dạng cho phép, bạn hãy chọn file có định dạng .jpg, gif hoặc png" });
                            }
                            //kiem tra dung luong file
                            var fileSize = file.ContentLength;
                            var fileMB = (fileSize / 1024f) / 1024f;
                            if (fileMB > 5)
                            {
                                return Json(new { success = 0, message = "Dung lượng ảnh không được lớn hơn 5MB" });
                            }
                            // luu ra dia
                            fileName = Path.GetFileNameWithoutExtension(file.FileName);
                            //fileName = fileName + "_" + user.UserID + extension;
                            fileName = fileName + extension;
                            var physicalPath = Path.Combine(Server.MapPath("~/Uploads/DuAn"), fileName);
                            file.SaveAs(physicalPath);
                            products.productInfo.ImageTitle = fileName;
                        }
                    }
                }
                products.productInfo.CreatedAt = DateTime.Now;
                products.productInfo.CreatedBy = User.Identity.Name;
                products.productInfo.Summary = products.Summary;
                products.productInfo.Summary_EN = products.Summary_EN;
                products.productInfo.Summary_JP = products.Summary_JP;
                products.productInfo.Description = products.Description;
                products.productInfo.Description_EN = products.Description_EN;
                products.productInfo.Description_JP = products.Description_JP;
                db.Products.Add(products.productInfo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(products);
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.ProductType = Utitlitys.ProductTypeToList();
            ViewBag.Status = Utitlitys.StatusToList();
            ViewBag.Platform = Utitlitys.PlatformToList();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Products products = db.Products.Find(id);
            if (products == null)
            {
                return HttpNotFound();
            }
            ProductViewModel viewmodel = new ProductViewModel();
            viewmodel.productInfo = products; 
            viewmodel.Description = products.Description;
            viewmodel.Description_EN = products.Description_EN;
            viewmodel.Description_JP = products.Description_JP;
            viewmodel.Summary = products.Summary;
            viewmodel.Summary_EN = products.Summary_EN;
            viewmodel.Summary_JP = products.Summary_JP;
            return View(viewmodel);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductViewModel products, IEnumerable<HttpPostedFileBase> files)
        {
            ViewBag.ProductType = Utitlitys.ProductTypeToList();
            ViewBag.Status = Utitlitys.StatusToList();
            ViewBag.Platform = Utitlitys.PlatformToList();
            if (ModelState.IsValid)
            {
                var old = db.Products.Find(products.productInfo.Id);
                var fileName = string.Empty;
                if (files != null)
                {
                    foreach (var file in files)
                    {
                        if (file != null && file.ContentLength > 0)
                        {
                            //kiem tra file extension lan nua
                            List<string> excelExtension = new List<string>();
                            excelExtension.Add(".png");
                            excelExtension.Add(".jpg");
                            excelExtension.Add(".gif");
                            var extension = Path.GetExtension(file.FileName);
                            if (!excelExtension.Contains(extension.ToLower()))
                            {
                                return Json(new { success = 0, message = "File không đúng định dạng cho phép, bạn hãy chọn file có định dạng .jpg, gif hoặc png" });
                            }
                            //kiem tra dung luong file
                            var fileSize = file.ContentLength;
                            var fileMB = (fileSize / 1024f) / 1024f;
                            if (fileMB > 5)
                            {
                                return Json(new { success = 0, message = "Dung lượng ảnh không được lớn hơn 5MB" });
                            }
                            if (old.ImageTitle != null)
                            {
                                fileName = old.ImageTitle;
                            }
                            else
                            {
                                // luu ra dia
                                fileName = Path.GetFileNameWithoutExtension(file.FileName);
                                //fileName = fileName + "_" + user.UserID + extension;
                                fileName = fileName + extension;
                            }

                            var physicalPath = Path.Combine(Server.MapPath("~/Uploads/DuAn"), fileName);
                            file.SaveAs(physicalPath);
                            products.productInfo.ImageTitle = fileName;
                            old.ImageTitle = products.productInfo.ImageTitle;
                        }
                    }
                }
                old.Name = products.productInfo.Name;
                old.Name_EN = products.productInfo.Name_EN;
                old.Name_JP = products.productInfo.Name_JP;
                old.Status = products.productInfo.Status;
                old.Type = products.productInfo.Type;
                old.Platform = products.productInfo.Platform;
                old.URL = products.productInfo.URL;
                old.Summary = products.Summary;
                old.Description = products.Description;
                old.Description_EN = products.Description_EN;
                old.Description_JP = products.Description_JP;
                old.Summary_EN = products.Summary_EN;
                old.Summary_JP = products.Summary_JP;
                old.UpdateAt = DateTime.Now;
                old.UpdateBy = User.Identity.Name;
                db.Entry(old).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(products);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Products products = db.Products.Find(id);
            if (products == null)
            {
                return HttpNotFound();
            }
            return View(products);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Products products = db.Products.Find(id);
            db.Products.Remove(products);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
