﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Susoft.Model;
using Susoft.CMS.Common;
using System.Net;

namespace Susoft.CMS.Controllers
{
    public class ProjectController : BaseController
    {
        private SusoftCMSEntities db = new SusoftCMSEntities();

        // GET: Project
        public ActionResult Index()
        {
            IList<Products> lstProduct = db.Products.Where(x=>x.Status == (int)CMS_STATUS.ACTIVE).OrderBy(x=>x.UpdateAt).ToList();
            return View(lstProduct);
        }

        // GET: Project/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Products product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}