﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace Susoft.CMS.Common
{
    public static class Utitlitys
    {
        public static SelectList StatusToList()
        {
            var status = new SelectListItem() { Text = "Phê duyệt", Value = ((int)CMS_STATUS.ACTIVE).ToString() };
            var lst = new List<SelectListItem>();
            lst.Add(status);
            status = new SelectListItem() { Text = "Chờ phê duyệt", Value = ((int)CMS_STATUS.PENDING).ToString() };
            lst.Add(status);
            status = new SelectListItem() { Text = "Đã xóa", Value = ((int)CMS_STATUS.DELETED).ToString() };
            lst.Add(status);
            return new SelectList(lst, "Value", "Text");
        }
        public static SelectList PlatformToList()
        {
            var platform = new SelectListItem() { Text = "Android", Value = ((int)CMS_PLATFORM.ANDOID).ToString() };
            var lst = new List<SelectListItem>();
            lst.Add(platform);
            platform = new SelectListItem() { Text = "IOS", Value = ((int)CMS_PLATFORM.IOS).ToString() };
            lst.Add(platform);
            platform = new SelectListItem() { Text = "Web", Value = ((int)CMS_PLATFORM.WEB).ToString() };
            lst.Add(platform);
            return new SelectList(lst, "Value", "Text");
        }
        public static SelectList PositionToList()
        {
            var position = new SelectListItem() { Text = "Top", Value = ((int)MENU_POSITION.TOP_MENU).ToString() };
            var lst = new List<SelectListItem>();
            lst.Add(position);
            position = new SelectListItem() { Text = "Left", Value = ((int)MENU_POSITION.LEFT_MENU).ToString() };
            lst.Add(position);
            position = new SelectListItem() { Text = "Right", Value = ((int)MENU_POSITION.RIGHT_MENU).ToString() };
            lst.Add(position);
            position = new SelectListItem() { Text = "Bottom", Value = ((int)MENU_POSITION.BOTTOM_MENU).ToString() };
            lst.Add(position);
            position = new SelectListItem() { Text = "All", Value = ((int)MENU_POSITION.ALL_POSITION).ToString() };
            lst.Add(position);
            return new SelectList(lst, "Value", "Text");
        }
        public static SelectList ProductTypeToList()
        {
            var producttype = new SelectListItem() { Text = "A", Value = ((int)CMS_PRODUCT_TYPE.A).ToString() };
            var lst = new List<SelectListItem>();
            lst.Add(producttype);
            producttype = new SelectListItem() { Text = "B", Value = ((int)CMS_PRODUCT_TYPE.B).ToString() };
            lst.Add(producttype);
            producttype = new SelectListItem() { Text = "C", Value = ((int)CMS_PRODUCT_TYPE.C).ToString() };
            lst.Add(producttype);
            producttype = new SelectListItem() { Text = "D", Value = ((int)CMS_PRODUCT_TYPE.D).ToString() };
            lst.Add(producttype);
            return new SelectList(lst, "Value", "Text");
        } 
        public static string PlatformToString(int platform)
        {
            var strResult = string.Empty;
            switch (platform)
            {
                case (int)CMS_PLATFORM.ANDOID:
                    {
                        strResult = "Android Development";
                        break;
                    }
                case (int)CMS_PLATFORM.IOS:
                    {
                        strResult = "IOS Development";
                        break;
                    }
                case (int)CMS_PLATFORM.WEB:
                    {
                        strResult = "Web development";
                        break;
                    }
                default: break; 
            }
            return strResult;
        }
        public static string ProductTypeToString(int producttype)
        {
            var strResult = string.Empty;
            switch (producttype)
            {
                case (int)CMS_PRODUCT_TYPE.A:
                    {
                        strResult = "A";
                        break;
                    }
                case (int)CMS_PRODUCT_TYPE.B:
                    {
                        strResult = "B";
                        break;
                    }
                case (int)CMS_PRODUCT_TYPE.C:
                    {
                        strResult = "C";
                        break;
                    }
                case (int)CMS_PRODUCT_TYPE.D:
                    {
                        strResult = "D";
                        break;
                    }
                default: break;
            }
            return strResult;
        }
        public static string PositionToString(int position)
        {
            var strResult = string.Empty;
            switch(position)
            {
                case (int)MENU_POSITION.TOP_MENU:
                    {
                        strResult = "Trên";
                        break;
                    }
                case (int)MENU_POSITION.LEFT_MENU:
                    {
                        strResult = "Trái";
                        break;
                    }
                case (int)MENU_POSITION.RIGHT_MENU:
                    {
                        strResult = "Phải";
                        break;
                    }
                case (int)MENU_POSITION.BOTTOM_MENU:
                    {
                        strResult = "Dưới";
                        break;
                    }
                case (int)MENU_POSITION.ALL_POSITION:
                    {
                        strResult = "All";
                        break;
                    }
                default: break;
            }
            return strResult;
        }
        public static string ImagePath(string fileName, int type)
        {
            if (type == (int)CMS_IMAGE_TYPE.ARTICLE_TYPE)
            {
                return string.Format("/Uploads/TinTuc/{0}", fileName);
            }
            else
            {
                return string.Format("/Uploads/DuAn/{0}", fileName);
            }
        }
        public static string StatusToString(int status)
        {
            var strResult = string.Empty;
            switch (status)
            {
                case (int)CMS_STATUS.ACTIVE:
                    {
                        strResult = "Đã duyệt";
                        break;
                    }
                case (int)CMS_STATUS.PENDING:
                    {
                        strResult = "Chờ phê duyệt";
                        break;
                    }
                case (int)CMS_STATUS.DELETED:
                    {
                        strResult = "Đã xóa";
                        break;
                    }
                default: break;
            }
            return strResult;
        }
        
        public static string DatetimeToVN(DateTime dt)
        {
            if (dt == null)
            {
                return "";
            }
            else
            {
                return dt.ToString("dd/MM/yyyy");
            }
           
        }
    }
}