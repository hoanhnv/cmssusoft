﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Susoft.CMS.Common
{

   public enum CMS_STATUS
    {
        ACTIVE = 1,
        PENDING = 2,
        DELETED = 3
    }
    public enum CMS_IMAGE_TYPE
    {
        ARTICLE_TYPE = 0,
        PRODUCT_TYPE = 1 
    }
    public enum CMS_PLATFORM
    {
        ANDOID =1,
        IOS =2,
        WEB =3
    }
    public enum MENU_POSITION
    {                 
        TOP_MENU = 0,
        LEFT_MENU = 1,
        RIGHT_MENU = 2,
        BOTTOM_MENU = 3,
        ALL_POSITION =4
    }
    public enum CMS_PRODUCT_TYPE
    {
        A = 0,
        B = 1,
        C = 3,
        D = 4
    }
}