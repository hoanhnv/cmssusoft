﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;

namespace Susoft.CMS.Common
{
    public abstract class BaseController : System.Web.Mvc.Controller
    {
        private string CurrentLanguageCode { get; set; }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            //if (requestContext.RouteData.Values["lang"] != null && requestContext.RouteData.Values["lang"] as string != "null")
            //{
            //    CurrentLanguageCode = (string)requestContext.RouteData.Values["lang"];
            //    if (CurrentLanguageCode != null)
            //    {
            //        try
            //        {
            //            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = new CultureInfo(CurrentLanguageCode);
            //        }
            //        catch (Exception)
            //        {
            //            throw new NotSupportedException($"Invalid language code '{CurrentLanguageCode}'.");
            //        }
            //    }
            //}
            base.Initialize(requestContext);
        }
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            //string cultureName = RouteData.Values["lang"] as string;

            //// Attempt to read the culture cookie from Request
            //if (cultureName == null)
            //    cultureName = Request.UserLanguages != null && Request.UserLanguages.Length > 0 ? Request.UserLanguages[0] : null; // obtain it from HTTP header AcceptLanguages

            //// Validate culture name
            //cultureName = CultureHelper.GetImplementedCulture(cultureName); // This is safe


            //if (RouteData.Values["lang"] as string != cultureName)
            //{

            //    // Force a valid culture in the URL
            //    RouteData.Values["lang"] = cultureName.ToLowerInvariant(); // lower case too
            //    // Redirect user
            //    Response.RedirectToRoute(RouteData.Values);
            //}


            //// Modify current thread's cultures            
            //Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
            //Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;


            return base.BeginExecuteCore(callback, state);
        }
    }
}